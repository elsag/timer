
class Incrementer extends React.Component{
    constructor(props){
        super(props)
        this.state={
            n:props.start,
            timer:null,
            
        }
    } 
    componentDidMount(){
        this.start()
    }
    componentWillUnmount(){
        this.pause()
    }

    start(){
        window.clearInterval(this.state.timer)
        this.setState({
            timer:window.setInterval(this.increment.bind(this),1000)
   
        })
    }

    pause(){

        window.clearInterval(this.state.timer)
        this.setState({
            timer:null
        })
    }

    increment(){
        this.setState({
            n: this.state.n + this.props.step
        })
    }

    toggle(){
       return this.state.timer ? this.pause() : this.start()
    }
    label(){
        return this.state.timer ? 'Pause' : 'Start'
    }

    restart(){
        this.pause()
        this.setState({
            n:0,
        })
        
    }

    render(){
        return <div class="timer">
            <p>Temps écoulé : {this.state.n} s</p>
            <div class="buttons">
                <button class="button" onClick={this.toggle.bind(this)}>{this.label()}</button> 
                <button class="button" onClick={this.restart.bind(this)}> Restart</button>
            </div>
        </div>
    }

}

Incrementer.defaultProps= {
    start:0,
    step:1
}

function Welcome(){
    return <h1>Welcome</h1>
}

ReactDOM.render(<Incrementer />,document.querySelector('#app'))